package com.example.nycschools_codechallenge_android.domain.usecase

import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.repository.NYCHighSchoolRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.modules.junit4.PowerMockRunner
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class NYCHighSchoolUseCaseTest {

    private lateinit var useCase: NYCHighSchoolUseCase

    @MockK
    lateinit var repository: NYCHighSchoolRepository

    @MockK
    lateinit var schools: List<School>

    private val provider = Dispatchers.IO

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        useCase = NYCHighSchoolUseCase(
            defaultDispatcher = provider,
            repository
        )
    }

    @Test
    fun `test on success`() = runBlocking {

        val success = Result.Success(schools)
        coEvery {
            repository.fetchNYCHighSchools()
        } returns success

        val result = useCase.invoke()

        assertTrue { result is Result.Success }
    }

    @Test
    fun `test on Error`() = runBlocking {

        val exception = java.lang.Exception()
        val failure = Result.Error(exception)
        coEvery {
            repository.fetchNYCHighSchools()
        } returns failure

        val result = useCase.invoke()

        assertTrue { result is Result.Error }
    }

    @Test
    fun `test on ApiError`() = runBlocking {

        val exception = java.lang.Exception()
        val failure = Result.ApiError(exception)
        coEvery {
            repository.fetchNYCHighSchools()
        } returns failure

        val result = useCase.invoke()

        assertTrue { result is Result.ApiError }
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}
