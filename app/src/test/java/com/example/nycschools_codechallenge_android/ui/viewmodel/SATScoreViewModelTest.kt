package com.example.nycschools_codechallenge_android.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools_codechallenge_android.common.CoroutineTestRule
import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.usecase.NYCHighSchoolUseCase
import com.example.nycschools_codechallenge_android.domain.usecase.SATScoreUseCase
import com.example.nycschools_codechallenge_android.view.model.SATScoreUiState
import com.example.nycschools_codechallenge_android.view.viewmodel.HomeViewModel
import com.example.nycschools_codechallenge_android.view.viewmodel.SATScoreViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.unmockkAll
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@ExperimentalCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
class SATScoreViewModelTest {

    @get:Rule
    var coroutineRule = CoroutineTestRule()

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private lateinit var underTests: SATScoreViewModel

    private val FAKE_ERROR = "UnknownError"

    @MockK
    lateinit var useCase: SATScoreUseCase

    @MockK
    lateinit var uiState: SATScoreUiState

    @MockK
    lateinit var satScore: SATScore

    private val FAKE_DBN = "XXXXX"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        underTests = SATScoreViewModel(useCase)
        every { uiState.satScore } answers { satScore }
    }

    @Test
    fun `test NYC schools success`() = runTest {
        val response = Result.Success(satScore)
        coEvery {
            useCase.invoke(FAKE_DBN)
        } coAnswers {
            response
        }

        underTests.getSATScore(FAKE_DBN)
        advanceUntilIdle()
        val result = underTests.satScoreResult.value
        assertEquals(result, uiState)
    }

    @Test
    fun `test NYC schools  failure`() = runTest {
        val errorObject = Exception(FAKE_ERROR)
        val response = Result.Error(errorObject)
        coEvery {
            useCase.invoke(FAKE_DBN)
        } coAnswers {
            response
        }

        underTests.getSATScore(FAKE_DBN)
        advanceUntilIdle()
        val result = underTests.errorMessage.value
        assertNotNull(result)
    }

    @Test
    fun `test NYC schools apiError`() = runTest {
        val errorObject = Exception(FAKE_ERROR)
        val response = Result.ApiError(errorObject)
        coEvery {
            useCase.invoke(FAKE_DBN)
        } coAnswers {
            response
        }

        underTests.getSATScore(FAKE_DBN)
        advanceUntilIdle()
        val result = underTests.errorMessage.value
        assertNotNull(result)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}
