package com.example.nycschools_codechallenge_android.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.nycschools_codechallenge_android.R
import com.example.nycschools_codechallenge_android.databinding.FragmentSchoolDetailsBinding
import com.example.nycschools_codechallenge_android.view.viewmodel.SATScoreViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * fragment to display SAT Score for a particular School.
 */
@AndroidEntryPoint
class SchoolDetailsFragment : Fragment() {

    private val viewModel: SATScoreViewModel by viewModels()
    private var _binding: FragmentSchoolDetailsBinding? = null
    private val binding get() = _binding!!
    private val args: SchoolDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewmodel = this.viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        val school = args.school
        viewModel.getSATScore(school.dbn)
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), R.string.no_record_found, Toast.LENGTH_LONG).show()
            findNavController().popBackStack()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}