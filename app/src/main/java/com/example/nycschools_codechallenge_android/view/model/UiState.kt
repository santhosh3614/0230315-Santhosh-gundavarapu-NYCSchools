package com.example.nycschools_codechallenge_android.view.model

import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School

data class SchoolsUiState (
    val schools: List<School> = listOf()
)

data class SATScoreUiState(
    val satScore: SATScore
)