package com.example.nycschools_codechallenge_android.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools_codechallenge_android.BR
import com.example.nycschools_codechallenge_android.data.model.NYCSchoolResult
import com.example.nycschools_codechallenge_android.databinding.NycschoolsRecyclerItemBinding
import com.example.nycschools_codechallenge_android.domain.model.School
import javax.inject.Inject

class NYCSchoolsListAdapter @Inject constructor(): ListAdapter<School,NYCSchoolsListAdapter.NYCSchoolViewHolder>(NYCSchoolComparator())  {

    var onSchoolClickListener:((School)-> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NYCSchoolViewHolder {
        return NYCSchoolViewHolder.create(parent, onSchoolClickListener)
    }

    override fun onBindViewHolder(holder: NYCSchoolViewHolder, position: Int) {
        val school = getItem(position)
        holder.bind(school)
    }

    class NYCSchoolViewHolder(val binding: NycschoolsRecyclerItemBinding, private val clickListener: ((School)-> Unit)?) : RecyclerView.ViewHolder(binding.root) {

        fun bind(school: School) {
            binding.setVariable(BR.school, school)
            binding.executePendingBindings()

            binding.nycschoolTextview.setOnClickListener{
                clickListener?.invoke(school)
            }
        }

        companion object {
            fun create(parent: ViewGroup, clickListener: ((School)-> Unit)?): NYCSchoolViewHolder {
                val binding = NycschoolsRecyclerItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                return NYCSchoolViewHolder(binding, clickListener)
            }
        }
    }

    class NYCSchoolComparator : DiffUtil.ItemCallback<School>() {
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.dbn == newItem.dbn
        }
    }
}