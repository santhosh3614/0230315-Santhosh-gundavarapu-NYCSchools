package com.example.nycschools_codechallenge_android

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolsApp: Application()