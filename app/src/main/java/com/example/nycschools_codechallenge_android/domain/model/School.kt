package com.example.nycschools_codechallenge_android.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    var dbn: String,
    var schoolName: String,
    var city: String? = null
): Parcelable