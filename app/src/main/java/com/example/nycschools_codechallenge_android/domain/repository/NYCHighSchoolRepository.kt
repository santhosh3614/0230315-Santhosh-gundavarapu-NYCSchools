package com.example.nycschools_codechallenge_android.domain.repository

import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School

interface NYCHighSchoolRepository {

    suspend fun fetchNYCHighSchools(): Result<List<School>>

    suspend fun fetchSATScore(dbn: String): Result<SATScore>
}