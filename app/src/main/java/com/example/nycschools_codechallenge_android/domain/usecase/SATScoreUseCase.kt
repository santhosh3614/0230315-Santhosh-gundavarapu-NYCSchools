package com.example.nycschools_codechallenge_android.domain.usecase

import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.repository.NYCHighSchoolRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SATScoreUseCase @Inject constructor(
    private val defaultDispatcher: CoroutineDispatcher,
    private val nycHighSchoolRepository: NYCHighSchoolRepository
) {
    suspend operator fun invoke(dbn: String): Result<SATScore> = withContext(Dispatchers.IO) {
        return@withContext nycHighSchoolRepository.fetchSATScore(dbn)
    }
}