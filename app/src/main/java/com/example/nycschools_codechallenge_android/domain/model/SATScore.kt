package com.example.nycschools_codechallenge_android.domain.model


data class SATScore(
    var dbn: String,
    var schoolName: String,
    var numOfSATTestTakers: String,
    var satCriticalReadingAvgScore: String,
    var satMathAvgScore: String,
    var satWritingAvgScore: String
)