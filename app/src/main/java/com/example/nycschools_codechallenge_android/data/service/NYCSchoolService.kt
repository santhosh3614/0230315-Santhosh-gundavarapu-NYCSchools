package com.example.nycschools_codechallenge_android.data.service

import com.example.nycschools_codechallenge_android.data.model.NYCSchoolResult
import com.example.nycschools_codechallenge_android.data.model.SATResult
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface NYCSchoolService {

    @GET("s3k6-pzi2.json")
    @Headers("X-App-Token: 1MVNSrY5oU7ROAnRz0IvBV6uL")
    suspend fun getAllNYCHighSchools(): List<NYCSchoolResult>

    @GET("f9bf-2cp4.json")
    @Headers("X-App-Token: 1MVNSrY5oU7ROAnRz0IvBV6uL")
    suspend fun getSATResults(@Query("dbn") dbn: String): List<SATResult>
}